---
- name: Ausführen von qm list, um alle VMs zu listen
  shell: qm list
  register: qm_output
  ignore_errors: true

- name: Überprüfen, ob die VM mit der ID 10000 existiert
  set_fact:
    vm_exists: "{{ '10000' in qm_output.stdout }}"

- name: Stelle sicher, dass das qcow2 Verzeichnis existiert
  file:
    path: /var/lib/vz/template/qcow2/
    state: directory
  when: not vm_exists

- name: Lade das Aktuelle Ubuntu {{ template_image_code_name }} Cloud-Image herunter
  get_url:
    url: "https://cloud-images.ubuntu.com/{{ template_image_code_name }}/current/{{ template_image_code_name }}-server-cloudimg-amd64.img"
    dest: "/var/lib/vz/template/qcow2/ubuntu-{{ template_image_code_name }}-template.img"
  when: not vm_exists

- name: Installiere libguestfs-tools package
  apt:
    pkg:
      - libguestfs-tools
    state: present
    update_cache: yes
  when: not vm_exists

- name: Installiere qemu-guest-agent in das Image
  command: >
    sudo virt-customize -a /var/lib/vz/template/qcow2/ubuntu-{{ template_image_code_name }}-template.img --install qemu-guest-agent
  when: not vm_exists

- name: Konvertiere img zu qcow2
  ansible.builtin.command:
    cmd: "qemu-img convert -O qcow2 /var/lib/vz/template/qcow2/ubuntu-{{ template_image_code_name }}-template.img /var/lib/vz/template/qcow2/ubuntu-{{ template_image_code_name }}-template.qcow2"
  when: not vm_exists

- name: Ändere Größe der qcow2-Datei auf {{ template_hdd_size }}
  ansible.builtin.command:
    cmd: "qemu-img resize /var/lib/vz/template/qcow2/ubuntu-{{ template_image_code_name }}-template.qcow2 {{ template_hdd_size }}"
  when: not vm_exists

- name: Erstelle neue VM in Proxmox
  shell: >
    sudo qm create {{ template_id }} --name "{{ template_name }}" --memory 2048 --cores 2 --net0 virtio,bridge=vmbr0
  when: not vm_exists

- name: Importiere die Festplatte zur VM
  shell: >
    sudo qm importdisk {{ template_id }} /var/lib/vz/template/qcow2/ubuntu-{{ template_image_code_name }}-template.qcow2 local -format qcow2
  when: not vm_exists

- name: Füge Disk zur Proxmox VM hinzu
  shell: "qm set {{ template_id }} -virtio0 local:{{ template_id }}/vm-{{ template_id }}-disk-0.qcow2"
  when: not vm_exists

- name: Setze Boot-Reihenfolge mit QEMU
  shell: qm set {{ template_id }} -boot order="virtio0;net0"
  when: not vm_exists

- name: Konfiguriere die VM
  shell: >
    sudo qm set {{ template_id }} --ide0 local:cloudinit &&
    sudo qm set {{ template_id }} --serial0 socket --vga serial0 &&
    sudo qm set {{ template_id }} --agent enabled=1 &&
    sudo qm set {{ template_id }} -boot order="virtio0;ide0;net0"
  when: not vm_exists

- name: Konvertiere VM zu einen Template
  shell: >
    sudo qm template {{ template_id }}
  when: not vm_exists