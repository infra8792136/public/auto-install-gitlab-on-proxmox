- [English](#automatic-installation-of-gitlab-with-gitlab-runner-on-proxmox)

# Automamtische Installation von Gitlab mit Gitlab-Runner auf PROXMOX

## Inhaltsverzeichnis

- [Über das Projekt](#über-das-projekt)
- [Voraussetzungen](#voraussetzungen)
- [Installation](#installation)
- [Verwendung](#verwendung)
- [Variable Konfiguration](#variable-konfiguration)
- [SSL Konfiguration Gitlab](#ssl-konfiguration-gitlab)
- [Zusätzliche Konfigurationsdateien ansible.cfg](#zusätzliche-konfigurationsdateien)
- [Lizenz](#lizenz)

## Über das Projekt

Dieses Projekt verwendet Ansible, um eine Reihe von Aufgaben zu automatisieren:

- Erstellung eines Proxmox-Templates
- Einrichtung einer GitLab-VM
- Installation von GitLab auf der VM
- Einrichtung eines GitLab-Runners in einem LXC-Container
- Aktualisierung von GitLab

## Voraussetzungen

- Installiertes Ansible
- Zugriff auf einen Proxmox-Host
- SSH-Zugriff auf die Ziele

## Installation

### Schritte

1. Klonen Sie dieses Repository.
2. Wechseln Sie in das Verzeichnis des Projekts.

```bash
cd auto-install-gitlab-on-proxmox
```

## Verwendung

### Ansible Playbook ausführen

Um das Ansible-Playbook zu starten, nutzen Sie den folgenden Befehl:

```bash
ansible-playbook --ask-vault-pass site.yml --tags "komplete-installation"
```

**⚠️ Achtung ⚠️**

Bevor Sie das Ansible-Playbook ausführen, stellen Sie sicher, dass Sie die Datei `ansible.cfg` und die Variablen in `vars.yml` und `vault.yml` entsprechend Ihren Anforderungen angepasst haben.


### Tags

Im Projekt sind verschiedene Tags definiert, um bestimmte Teile des Playbooks gezielt auszuführen:

- `komplete-installation`: Führt alle Rollen und Tasks im Playbook aus bis auf Gitlab-update.
- `template-erstellen`: Nur die Rolle `proxmox-template` wird ausgeführt.
- `vm-erstellen`: Nur die Rolle `proxmox-vm` wird ausgeführt.
- `gitlab-installation`: Nur die Rolle `gitlab-install` wird ausgeführt.
- `lxc-erstellen`: Nur die Rolle `proxmox-lxc` wird ausgeführt.
- `gitlab-runner-installation`: Rollen `proxmox-lxc` und `gitlab-runner-install` werden ausgeführt.
- `gitlab-update`: Nur die Rolle `gitlab-update` wird ausgeführt.


## Variable Konfiguration

Konfigurationsvariablen finden Sie in `vars.yml` und sensitive Daten in `vault.yml`.


### Allgemeine Variablen

| Variable      | Beispiel-Wert   | Beschreibung                  |
|---------------|----------------|-------------------------------|
| `gateway`     | `192.168.1.1`| Gateway-Adresse               |
| `dns_server`  | `192.168.1.2`| DNS-Serveradresse             |
| `domain`      | `example.de`    | Domain-Name                   |

### Variablen für Proxmox

| Variable            | Beispiel-Wert       | Beschreibung                       |
|---------------------|--------------------|------------------------------------|
| `proxmox_api_host`  | `192.168.1.230`  | Proxmox API Hostadresse            |
| `proxmox_api_user`  | `admin@pve`        | Proxmox API Benutzer               |
| `proxmox_node`      | `proxmox`          | Proxmox Node                       |

### Variablen für Proxmox Ubuntu Template

| Variable                  | Beispiel-Wert               | Beschreibung                               |
|---------------------------|----------------------------|--------------------------------------------|
| `template_id`             | `10000`                    | Template-ID                                |
| `template_name`           | `Template-Ubuntu-jammy-Cloud`| Template-Name                         |
| `template_image_code_name`| `jammy`                    | Ubuntu Code-Name (z.B. jammy für 22.04)    |
| `template_hdd_size`       | `200G`                     | Festplattengröße des Templates             |

### Variablen für Proxmox Ubuntu-Gitlab VM

| Variable      | Beispiel-Wert      | Beschreibung                  |
|---------------|-------------------|-------------------------------|
| `vm_id`       | `1001`            | VM-ID                         |
| `vm_hostname` | `gitlab.example.de`| VM-Hostname                   |
| `vm_name`     | `gitlab`         | VM-Name                       |
| `vm_user`     | `dev`             | VM-Benutzer                   |
| `vm_ip`       | `192.168.1.231` | IP-Adresse der VM             |
| `vm_cores`    | `4`               | Anzahl der CPU-Kerne          |
| `vm_memory`   | `16384`           | Arbeitsspeicher in MB         |
| `vm_hdd`      | `400`             | Festplattengröße in GB        |

### Variablen für Proxmox Ubuntu-Gitlab-Runner Container

| Variable         | Beispiel-Wert     | Beschreibung                  |
|------------------|-------------------|-------------------------------|
| `lxc_name`       | `gitlab-runner`   | Container-Name                |
| `lxc_ip`         | `192.168.1.232` | IP-Adresse des Containers     |
| `lxc_id`         | `1002`            | Container-ID                  |
| `lxc_cores`      | `2`               | Anzahl der CPU-Kerne          |
| `lxc_memory`     | `4096`            | Arbeitsspeicher in MB         |
| `lxc_swap`       | `2048`            | Swap-Größe in MB              |
| `lxc_hdd_size`   | `100G`            | Festplattengröße              |
| `lxc_image_version` | `ubuntu-22.04` | Ubuntu-Version für den Container |

### Variablen aus der Vault

| Variable              | Beispiel-Wert  | Beschreibung                        |
|-----------------------|---------------|-------------------------------------|
| `proxmox_api_password`| `Test123!`    | Passwort für Proxmox API            |
| `vm_password`         | `Test123!`    | Passwort für die VM                 |
| `gitlab_root_password`| `Test123!`    | Root-Passwort für GitLab            |
| `gitlab_personal_token`| `Test123!`   | Persönlicher Zugangstoken für GitLab|
| `lxc_password`        | `Test123!`    | Passwort für den LXC-Container      |
| `local_password`      | `Test123!`    | Lokales Passwort                    |
| `sshkey`              | `ssh-rsa` | SSH-Schlüssel                    |


### SSL Konfiguration Gitlab

#### Beschreibung

Diese Rolle kümmert sich um die Installation von GitLab auf einer Ubuntu-basierten VM. Sie setzt voraus, dass bereits eine VM existiert, auf der GitLab installiert werden soll.

#### Spezielle Variablen für gitlab-install

| Variable              | Beispielwert      | Beschreibung                    |
|-----------------------|-------------------|---------------------------------|
| `vm_name`             | `gitlab`         | Name der VM für GitLab          |
| `domain`              | `example.de`       | Domain der VM                   |

#### Zertifikat und Schlüsseldatei

Für die Installation werden ein SSL-Zertifikat und ein Schlüssel benötigt. Diese sollten in folgenden Formaten und Pfaden verfügbar sein:

- Zertifikatsdatei: `{{ vm_name }}.{{ domain }}.crt`
- Schlüsseldatei: `{{ vm_name }}.{{ domain }}.key`

Diese Dateien sollten im Ordner `roles/gitlab-install/files/` abgelegt werden. Wenn die Dateien nicht vorhanden sind, werden selbst signierte Zertifikate erstellt.

## Zusätzliche Konfigurationsdateien

### ansible.cfg

Die `ansible.cfg`-Datei enthält grundlegende Einstellungen für die Ausführung von Ansible. Hier eine kurze Erklärung der wichtigsten Parameter:

#### Sektion `[defaults]`

- `inventory`: Pfad zur Inventardatei. Standardmäßig auf `./inventory.ini` gesetzt.
- `remote_user`: Benutzername für den SSH-Zugang. In diesem Projekt ist es `dev`.
- `private_key_file`: Pfad zum privaten SSH-Schlüssel. Hier ist es `~/.ssh/<Name-SSH-Private-Key`.
- `log_path`: Pfad zur Log-Datei für Ansible-Aktionen. Hier ist es `./ansible.log`.
- `host_key_checking`: Überprüfung des SSH-Host-Schlüssels deaktivieren. Eingestellt auf `False` (nicht empfohlen für Produktivumgebungen).

#### Sektion `[ssh_connection]`

- `ssh_args`: Zusätzliche SSH-Optionen. Hier ist `-o HostKeyAlgorithms=+ssh-rsa` eingestellt, um den verwendeten HostKeyAlgorithmus explizit zu spezifizieren.

Diese Einstellungen können Sie in Ihre README.md-Datei integrieren, um eine umfassende Dokumentation Ihres Projekts sicherzustellen.

## Lizenz

Dieses Projekt steht unter der MIT-Lizenz - siehe die [LICENSE.md](LICENSE.md) Datei für weitere Details.


---
# Automatic Installation of Gitlab with Gitlab-Runner on PROXMOX

## Table of Contents

- [About the Project](#about-the-project)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Variable Configuration](#variable-configuration)
- [SSL Configuration for Gitlab](#ssl-configuration-for-gitlab)
- [Additional Configuration Files ansible.cfg](#additional-configuration-files)
- [License](#license)

## About the Project

This project uses Ansible to automate a series of tasks:

- Creating a Proxmox template
- Setting up a GitLab VM
- Installing GitLab on the VM
- Setting up a GitLab-Runner in an LXC container
- Updating GitLab

## Prerequisites

- Installed Ansible
- Access to a Proxmox host
- SSH access to the targets

## Installation

### Steps

1. Clone this repository.
2. Navigate to the project directory.

```bash
cd auto-install-gitlab-on-proxmox
```

## Usage

### Run Ansible Playbook

To start the Ansible playbook, use the following command:

```bash
ansible-playbook --ask-vault-pass site.yml --tags "komplete-installation"
```

**⚠️ Warning ⚠️**

Before running the Ansible playbook, make sure to adjust the `ansible.cfg` file and the variables in `vars.yml` and `vault.yml` according to your requirements.

### Tags

Various tags are defined in the project to selectively run certain parts of the playbook:

- `komplete-installation`: Executes all roles and tasks in the playbook except GitLab-update.
- `template-erstellen`: Only the role `proxmox-template` will be executed.
- `vm-erstellen`: Only the role `proxmox-vm` will be executed.
- `gitlab-installation`: Only the role `gitlab-install` will be executed.
- `lxc-erstellen`: Only the role `proxmox-lxc` will be executed.
- `gitlab-runner-installation`: Roles `proxmox-lxc` and `gitlab-runner-installation` will be executed.
- `gitlab-update`: Only the role `gitlab-update` will be executed.

## Variable Configuration

You can find configuration variables in `vars.yml` and sensitive data in `vault.yml`.

### General Variables

| Variable      | Example Value  | Description                   |
|---------------|----------------|-------------------------------|
| `gateway`     | `192.168.1.1`| Gateway Address               |
| `dns_server`  | `192.168.1.2`| DNS Server Address            |
| `domain`      | `example.de`   | Domain Name                   |

### Variables for Proxmox

| Variable            | Example Value    | Description                     |
|---------------------|------------------|---------------------------------|
| `proxmox_api_host`  | `192.168.1.230`| Proxmox API Host Address        |
| `proxmox_api_user`  | `admin@pve`      | Proxmox API User                |
| `proxmox_node`      | `proxmox`        | Proxmox Node                    |

### Variables for Proxmox Ubuntu Template

| Variable                   | Example Value            | Description                             |
|----------------------------|--------------------------|-----------------------------------------|
| `template_id`              | `10000`                  | Template ID                             |
| `template_name`            | `Template-Ubuntu-jammy-Cloud`| Template Name                     |
| `template_image_code_name` | `jammy`                  | Ubuntu Code Name (e.g., jammy for 22.04)|
| `template_hdd_size`        | `200G`                   | Template HDD Size                       |

### Variables for Proxmox Ubuntu-Gitlab VM

| Variable      | Example Value     | Description                   |
|---------------|-------------------|-------------------------------|
| `vm_id`       | `1001`            | VM ID                         |
| `vm_hostname` | `gitlab.example.de`| VM Hostname                   |
| `vm_name`     | `gitlab`          | VM Name                       |
| `vm_user`     | `dev`             | VM User                       |
| `vm_ip`       | `192.168.1.231` | VM IP Address                 |
| `vm_cores`    | `4`               | Number of CPU Cores           |
| `vm_memory`   | `16384`           | RAM in MB                     |
| `vm_hdd`      | `400`             | HDD Size in GB                |

### Variables for Proxmox Ubuntu-Gitlab-Runner Container

| Variable         | Example Value    | Description                   |
|------------------|------------------|-------------------------------|
| `lxc_name`       | `gitlab-runner`  | Container Name                |
| `lxc_ip`         | `192.168.1.232`| Container IP Address          |
| `lxc_id`         | `1002`           | Container ID                  |
| `lxc_cores`      | `2`              | Number of CPU Cores           |
| `lxc_memory`     | `4096`           | RAM in MB                     |
| `lxc_swap`       | `2048`           | Swap Size in MB               |
| `lxc_hdd_size`   | `100G`           | HDD Size                      |
| `lxc_image_version` | `ubuntu-22.04`| Ubuntu Version for the Container |

### Variables from the Vault

| Variable              | Example Value | Description                       |
|-----------------------|---------------|-----------------------------------|
| `proxmox_api_password`| `Test123!`    | Password for Proxmox API          |
| `vm_password`         | `Test123!`    | Password for the VM               |
| `gitlab_root_password`| `Test123!`    | Root Password for GitLab          |
| `gitlab_personal_token`| `Test123!`   | Personal Access Token for GitLab  |
| `lxc_password`        | `Test123!`    | Password for the LXC Container    |
| `local_password`      | `Test123!`    | Local Password                    |
| `sshkey`              | `ssh-rsa`     | SSH Key                           |

### SSL Configuration Gitlab

#### Description

This role takes care of the installation of GitLab on an Ubuntu-based VM. It assumes that a VM already exists where GitLab is to be installed.

#### Special Variables for gitlab-install

| Variable              | Example Value     | Description                     |
|-----------------------|-------------------|---------------------------------|
| `vm_name`             | `gitlab`          | Name of the VM for GitLab       |
| `domain`              | `example.de`      | Domain of the VM                |

#### Certificate and Key File

For the installation, an SSL certificate and a key are required. These should be available in the following formats and paths:

- Certificate file: `{{ vm_name }}.{{ domain }}.crt`
- Key file: `{{ vm_name }}.{{ domain }}.key`

These files should be placed in the folder `roles/gitlab-install/files/`. If the files are not present, self-signed certificates will be generated during the installation.

## Additional Configuration Files

### ansible.cfg

The `ansible.cfg` file contains basic settings for running Ansible. Here is a brief explanation of the key parameters:

#### `[defaults]` Section

- `inventory`: Path to the inventory file. Default is `./inventory.ini`.
- `remote_user`: Username for SSH access. In this project, it's `dev`.
- `private_key_file`: Path to the private SSH key. Here, it's `~/.ssh/<Name-of-SSH-Private-Key>`.
- `log_path`: Path to the log file for Ansible actions. Here, it's `./ansible.log`.
- `host_key_checking`: Disable SSH host key checking. Set to `False` (not recommended for production environments).

#### `[ssh_connection]` Section

- `ssh_args`: Additional SSH options. Here, it is set to `-o HostKeyAlgorithms=+ssh-rsa` to explicitly specify the used HostKeyAlgorithm.

These settings can be integrated into your README.md file to ensure comprehensive documentation of your project.

## License

This project is under the MIT License - see the [LICENSE.md](LICENSE.md) file for more details.